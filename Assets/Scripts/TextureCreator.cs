﻿using UnityEngine;

public class TextureCreator : MonoBehaviour
{
    [Range(2, 512)]
    public int resolution = 256;

    public float frequency = 1f;

    [Range(0f, 2f)]
    public float amplitude = 1f;

    [Range(1, 8)]
    public int octaves = 1;

    [Range(1f, 4f)]
    public float lacunarity = 2f;

    [Range(0f, 1f)]
    public float persistence = 0.5f;

    [Range(1, 3)]
    public int dimensions = 3;

    public NoiseMethodType type;

    public Gradient coloring;


    private Texture2D texture;

    private float[,] noiseData;

    private void OnEnable()
    {
        if (texture == null)
        {
            texture = new Texture2D(resolution, resolution, TextureFormat.RGB24, true);
            texture.name = "Procedural Texture";
            texture.wrapMode = TextureWrapMode.Clamp;
            texture.filterMode = FilterMode.Trilinear;
            texture.anisoLevel = 9;
            GetComponent<MeshRenderer>().material.mainTexture = texture;
        }
        FillTexture();
    }

    public void UpdateResolution(UnityEngine.UI.Slider slider)
    {
        switch (slider.name)
        {
            case "Slider_Resolution":
                resolution = (int)slider.value;
                break;
            case "Slider_Frequency":
                frequency = slider.value;
                break;
            case "Slider_Amplitude":
                amplitude = slider.value;
                break;
            case "Slider_Octaves":
                octaves = (int)slider.value;
                break;
            case "Slider_Lacunarity":
                lacunarity = slider.value;
                break;
            case "Slider_Persistence":
                persistence = slider.value;
                break;
            default:
                break;
        }

        GameObject.FindGameObjectWithTag("Hud").GetComponent<HudNoise>().UpdateText();
        FillTexture();
        GameObject.FindGameObjectWithTag("Grid").GetComponent<TerrainGenerator>().GenerateTerrain();
    }

    private void Update()
    {
        if (transform.hasChanged)
        {
            transform.hasChanged = false;
            FillTexture();
        }
    }

    // Generate texture using Perlin Noise
    public void FillTexture()
    {
        if (texture.width != resolution)
        {
            texture.Resize(resolution, resolution);
           
        }
        noiseData = new float[texture.width, texture.height];

        Vector3 point00 = transform.TransformPoint(new Vector3(-0.5f, -0.5f));
        Vector3 point10 = transform.TransformPoint(new Vector3(0.5f, -0.5f));
        Vector3 point01 = transform.TransformPoint(new Vector3(-0.5f, 0.5f));
        Vector3 point11 = transform.TransformPoint(new Vector3(0.5f, 0.5f));

        NoiseMethod method = Noise.methods[(int)type][dimensions - 1];
        float stepSize = 1f / resolution;
        for (int y = 0; y < resolution; y++)
        {
            Vector3 point0 = Vector3.Lerp(point00, point01, (y + 0.5f) * stepSize);
            Vector3 point1 = Vector3.Lerp(point10, point11, (y + 0.5f) * stepSize);
            for (int x = 0; x < resolution; x++)
            {
                Vector3 point = Vector3.Lerp(point0, point1, (x + 0.5f) * stepSize);
                float sample = Noise.Sum(method, point, frequency, amplitude, octaves, lacunarity, persistence);
                noiseData[x, y] = sample; // sample * -1f makes color gradient to inverse. It's more intuitive.
                if (type != NoiseMethodType.Value)
                {
                    sample = sample * 0.5f + 0.5f;
                }
                texture.SetPixel(x, y, coloring.Evaluate(sample));
            }
        }
        texture.Apply();
    }

    // Update texture to correspond updated noiseData[,]
    public void UpdateTexture(float[,] noiseData)
    {
        for (int y = 0; y < resolution; y++)
        {
            for (int x = 0; x < resolution; x++)
            {
                float sample = noiseData[x, y];
                noiseData[x, y] = sample;
                if (type != NoiseMethodType.Value)
                {
                    sample = sample * 0.5f + 0.5f;
                }
                texture.SetPixel(x, y, coloring.Evaluate(sample));
            }
        }
        texture.Apply();
    }

    public Texture2D GetTexture()
    {
        return texture;
    }

    public float[,] GetNoiseData()
    {
        return noiseData;
    }

    public Gradient GetGradient()
    {
        return coloring;
    }
}