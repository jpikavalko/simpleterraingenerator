﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    // Classes
    ProceduralGrid grid;
    TextureCreator textureCreator;

    // Components
    MeshRenderer meshRenderer;
    MeshCollider meshCollider;

    private void Awake()
    {
        grid = GetComponent<ProceduralGrid>();
        meshRenderer = GetComponent<MeshRenderer>();
        meshCollider = GetComponent<MeshCollider>();

        textureCreator = GameObject.Find("Quad").GetComponent<TextureCreator>();
    }

    void Start()
    {
        GenerateTerrain();
    }

    public void GenerateTerrain()
    {
        Texture2D texture = textureCreator.GetTexture();

        grid.GenerateWithNoiseData(textureCreator.GetNoiseData());
        meshRenderer.material.mainTexture = texture;

        UpdateMeshCollider();
    }

    public void UpdateMeshCollider()
    {
        meshCollider.sharedMesh = grid.GetMesh();
    }
}
