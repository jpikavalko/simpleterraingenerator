﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerraformingTool : MonoBehaviour
{
    Camera cam;
    MeshFilter meshFilter;
    ProceduralGrid grid;
    TextureCreator textureCreator;
    TerrainGenerator generator;
    HudNoise hud;

    bool lowerTerrain = true;
    int brushSize = 1;



    void Start()
    {
        cam = Camera.main;
        meshFilter = GetComponent<MeshFilter>();
        grid = GetComponent<ProceduralGrid>();
        generator = GetComponent<TerrainGenerator>();
        hud = GameObject.FindGameObjectWithTag("Hud").GetComponent<HudNoise>();

        textureCreator = GameObject.FindGameObjectWithTag("Quad").GetComponent<TextureCreator>();
        if (!textureCreator)
        {
            Debug.LogWarning("Texture Creator not found");
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);

        // Brush terraforming direction
        if (Input.GetMouseButtonDown(1))
        {
            lowerTerrain = !lowerTerrain;
        }

        // Brush size
        if ((int)Input.mouseScrollDelta.y != 0)
        {
            brushSize += (int)Input.mouseScrollDelta.y;
            brushSize = Mathf.Clamp(brushSize, 1, 5);
        }

        // Update brush info
        hud.UpdateBrushInfo(brushSize, lowerTerrain);

        if (Physics.Raycast(ray, out hit))
        {
            if (Input.GetMouseButton(0))
            {
                TerraformGrid(hit);
            }
        }

        // Update mesh collider when mouse is up
        if (Input.GetMouseButtonUp(0))
        {
            generator.UpdateMeshCollider();
        }
    }

    private void TerraformGrid(RaycastHit hit)
    {
        // 1. Get noise data (from TextureCreator)
        float[,] noiseData = textureCreator.GetNoiseData();

        // 2. Get hit.point
        Vector3 hitPos = hit.point;

        for (int x = -brushSize; x <= brushSize; x++)
        {
            for (int y = -brushSize; y <= brushSize; y++)
            {
                if (hitPos.x + x >= noiseData.GetLength(0) || hitPos.x + x < 0 || hitPos.z + y >= noiseData.GetLength(1) || hitPos.z + y < 0) continue; // continue if index out of bounds

                Debug.Log(noiseData.GetLength(0));

                // 3. Manipulate noiseData
                if (lowerTerrain)   noiseData[Mathf.FloorToInt(hitPos.x) + x, Mathf.FloorToInt(hitPos.z) + y] += 0.01f; // TODO: Make formula to calculate position when texture resolution is something else than 128
                else                noiseData[Mathf.FloorToInt(hitPos.x) + x, Mathf.FloorToInt(hitPos.z) + y] += -0.01f;
            }
        }

        // 4. Generate new terrain with this data.
        grid.GenerateWithNoiseData(noiseData);

        // 5. Update texture
        textureCreator.UpdateTexture(noiseData);
    }

    public int GetClosestVertex(RaycastHit hit, int[] aTriangles)
    {
        Vector3 b = hit.barycentricCoordinate;
        int index = hit.triangleIndex * 3;

        if (aTriangles == null || index < 0 || index + 2 >= aTriangles.Length)
            return -1;

        if (b.x > b.y)
        {
            if (b.x > b.z)
                return aTriangles[index]; // x
            else
                return aTriangles[index + 2]; // z
        }
        else if (b.y > b.z)
            return aTriangles[index + 1]; // y
        else
            return aTriangles[index + 2]; // z
    }
}