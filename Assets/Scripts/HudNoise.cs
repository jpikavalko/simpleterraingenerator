﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudNoise : MonoBehaviour
{
    // Classes
    TerrainGenerator grid;
    TextureCreator noiseGen;

    // UI elements
    public Text noiseInfo;
    public Text brushInfo;
    RawImage hudImage;

    // Start is called before the first frame update
    void Start()
    {
        hudImage = GetComponent<RawImage>();
        noiseGen = GameObject.FindGameObjectWithTag("Quad").GetComponent<TextureCreator>();
        hudImage.texture = noiseGen.GetTexture();
        grid = GameObject.FindGameObjectWithTag("Grid").GetComponent<TerrainGenerator>();
        UpdateText();
    }

    public void GenerateNew()
    {
        grid.GenerateTerrain();
        hudImage.texture = noiseGen.GetTexture();
    }

    public void UpdateText()
    {
        noiseInfo.text =
            noiseGen.resolution.ToString() + "\n" +
            noiseGen.frequency.ToString() + "\n" +
            noiseGen.amplitude.ToString() + "\n" +
            noiseGen.octaves.ToString() + "\n" +
            noiseGen.lacunarity.ToString() + "\n" +
            noiseGen.persistence.ToString() + "\n" +
            noiseGen.type.ToString() + "\n";
    }

    public void UpdateBrushInfo(int brushSize, bool lower)
    {
        string raiselower = "Dig down";
        if (lower)  raiselower = "Dig down";
        else        raiselower = "Raise up";

        brushInfo.text =
            "Terraforming\n" +
            "BrushSize: " +brushSize + "\n" +
            raiselower;
    }
}
